package com.arnyminerz.escalaralcoiaicomtat.async

class FirebaseException(val code: String, message: String) : Exception(message)