package com.arnyminerz.escalaralcoiaicomtat.async

const val EXTENDED_API_URL = "https://api.arnyminerz.com"
const val EXTENDED_API_URL_NO_SECURE = "http://api.arnyminerz.com"