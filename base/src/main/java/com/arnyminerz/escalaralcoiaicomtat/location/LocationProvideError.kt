package com.arnyminerz.escalaralcoiaicomtat.location

enum class LocationProvideError {
    OK,
    GPS_DISABLED,
    NO_PERMISSION
}