package com.arnyminerz.escalaralcoiaicomtat.generic

data class ValueMax<T : Number>(val value: T, val max: T)