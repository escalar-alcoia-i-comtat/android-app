package com.arnyminerz.escalaralcoiaicomtat.exception

import java.lang.Exception

class AlreadyLoadingException: Exception("Content already loading")