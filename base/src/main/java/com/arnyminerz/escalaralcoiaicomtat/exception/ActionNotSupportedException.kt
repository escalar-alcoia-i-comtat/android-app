package com.arnyminerz.escalaralcoiaicomtat.exception

class ActionNotSupportedException(message: String) : Exception(message)