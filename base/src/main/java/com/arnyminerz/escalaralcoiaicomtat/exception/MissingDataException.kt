package com.arnyminerz.escalaralcoiaicomtat.exception

open class MissingDataException(msg: String) : Exception(msg)