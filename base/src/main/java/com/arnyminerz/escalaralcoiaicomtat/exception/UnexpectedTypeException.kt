package com.arnyminerz.escalaralcoiaicomtat.exception

import java.lang.Exception

class UnexpectedTypeException(msg: String): Exception(msg)