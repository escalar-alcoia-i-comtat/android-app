package com.arnyminerz.escalaralcoiaicomtat.exception

class NotLoggedInException : Exception("User not logged in")