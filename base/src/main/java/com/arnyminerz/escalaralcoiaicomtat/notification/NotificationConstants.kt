package com.arnyminerz.escalaralcoiaicomtat.notification

const val DOWNLOAD_PROGRESS_CHANNEL_ID = "DownloadProgress"
const val DOWNLOAD_COMPLETE_CHANNEL_ID = "DownloadComplete"
const val ALERT_CHANNEL_ID = "Alert"
const val TASK_COMPLETED_CHANNEL_ID = "TaskCompleted"
const val FRIEND_REQUEST_CHANNEL_ID = "FriendRequest"
const val BETA_UPDATE_CHANNEL_ID = "BetaUpdate"
const val FRIEND_REQUEST_ACCEPTED_CHANNEL_ID = "FriendRequestAccepted"
const val FRIEND_REMOVED_CHANNEL_ID = "FriendRemovedAccepted"
const val USER_INTERACTED_CHANNEL_ID = "BetaUpdate"
const val UPDATE_AVAILABLE_CHANNEL_ID = "NewUpdate"

const val DOWNLOADS_NOTIFICATION_CHANNEL_GROUP = "downloads_notifications_group"
const val PEOPLE_NOTIFICATION_CHANNEL_GROUP = "people_notifications_group"